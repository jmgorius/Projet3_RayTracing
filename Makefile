.PHONY: default all clean mrproper astyle doc zip

SDL_CFLAGS := $(shell sdl-config --cflags)
SDL_LIBS := $(shell sdl-config --libs)

SOURCE_DIR	:= ./src
SCREEN_DIR	:= $(SOURCE_DIR)/Screen
SCENE_DIR	:= $(SOURCE_DIR)/Scene
CORE_DIR	:= $(SOURCE_DIR)/Core

CC	:= g++
CFLAGS	:= -O3 -W -Wall -Wno-unused-parameter -Wno-unused-function -ansi -std=c++14 $(SDL_CFLAGS)
LDFLAGS	:=

INCLS	:= -I. -I$(SCREEN_DIR) -I$(SCENE_DIR) -I$(CORE_DIR)
LIBS	:= $(SDL_LIBS)

TARGET	:= main

CPP_FILES	:= $(wildcard $(SOURCE_DIR)/*.cpp $(SCREEN_DIR)/*.cpp $(SCENE_DIR)/*.cpp $(CORE_DIR)/*.cpp)
HPP_FILES	:= $(wildcard $(SOURCE_DIR)/*.hpp $(SCREEN_DIR)/*.hpp $(SCENE_DIR)/*.hpp $(CORE_DIR)/*.hpp)
OBJ_FILES	:= $(CPP_FILES:%.cpp=%.o)

default: all

all: $(TARGET)

# Documentation on Automatics Variables:
# https://www.gnu.org/software/make/manual/make.html#Automatic-Variables

$(TARGET): $(OBJ_FILES) Makefile
	$(CC) $(LDFLAGS) $(OBJ_FILES) $(LIBS) -o $@

$(OBJ_FILES): %.o: %.cpp Makefile $(HPP_FILES)
	$(CC) $(CFLAGS) $(INCLS) -c $< -o $@

### Begin inclusion part

# Documentation on processing include directives
# https://www.gnu.org/software/make/manual/make.html#Include

main.dep: $(CPP_FILES) $(HPP_FILES)
	gcc -MM -std=c++14 $(INCLS) $^ > main.dep

include main.dep

# End inclusion part

clean:
	-rm -f $(OBJ_FILES) main.dep
	-find . -type f -iname '*.orig' -delete
	
mrproper: clean
	-rm -f $(TARGET)
	-rm -rf doc/html

astyle:
	-astyle $(CPP_FILES) $(HPP_FILES)
	
doc:
	-doxygen doc/Doxyfile

zip: astyle clean
	zip -r ../raytracing.zip Makefile *.?pp Screen/*.?pp Scene/object.hpp Scene/camera.hpp
