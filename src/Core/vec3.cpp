/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "vec3.hpp"

#include <cmath>

vec3::vec3(double _x, double _y, double _z) :
    x(_x), y(_y), z(_z)
{
}

vec3& vec3::operator*=(double t)
{
    x *= t;
    y *= t;
    z *= t;
    return *this;
}

vec3& vec3::operator/=(double t)
{
    x /= t;
    y /= t;
    z /= t;
    return *this;
}

vec3& vec3::operator+=(const vec3& rhs)
{
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
}

vec3& vec3::operator-=(const vec3& rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
}

vec3 operator*(double t, vec3 v)
{
    v *= t;
    return v;
}

vec3 operator/(vec3 v, double t)
{
    v /= t;
    return v;
}

vec3 operator+(vec3 lhs, const vec3& rhs)
{
    lhs += rhs;
    return lhs;
}

vec3 operator-(vec3 lhs, const vec3& rhs)
{
    lhs -= rhs;
    return lhs;
}

vec3 vec3::operator-()
{
    vec3 res;
    res.x = -x;
    res.y = -y;
    res.z = -z;
    return res;
}

double dot(const vec3& v1, const vec3& v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

vec3 cross(const vec3& v1, const vec3& v2)
{
    return vec3(
               v1.y * v2.z - v1.z * v2.y,
               v1.z * v2.x - v1.x * v2.z,
               v1.x * v2.y - v1.y * v2.x
           );
}

double vec3::length() const
{
    return sqrt(dot(*this, *this));
}

vec3 normalize(vec3 v)
{
    v /= v.length();
    return v;
}
