/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Core/vec3.hpp
 * @brief vec3 class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of a class representing a 3-dimensional vector.
 *
 */

#pragma once

/**
 * @brief vec3 class
 *
 * Represents a 3-dimensional vector.
 *
 */
struct vec3
{
    /**
     * @brief Constructor
     *
     * Default constructor.
     *
     */
    vec3() = default;

    /**
     * @brief Constructor
     *
     * Create a vector from three coordinates.
     *
     * @param x : x coordinate.
     * @param y : y coordinate.
     * @param z : z coordinate.
     */
    vec3(double x, double y, double z);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Vector to copy.
     */
    vec3(const vec3& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~vec3() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Vector to assign to the current one.
     *
     * @return Reference to @a *this.
     */
    vec3& operator=(const vec3& rhs) = default;

    /**
     * @brief Addition assignment operator
     *
     * Add @a rhs to the current vector.
     *
     * @param rhs : Vector to add.
     *
     * @return Reference to @a *this.
     */
    vec3& operator+=(const vec3& rhs);

    /**
     * @brief Substraction assignment operator
     *
     * Substract @a rhs from the current vector.
     *
     * @param rhs : Vector to substract.
     *
     * @return Reference to @a *this.
     */
    vec3& operator-=(const vec3& rhs);

    /**
     * @brief Scalar multiply assignment operator
     *
     * Multiply the current vector by @a rhs.
     *
     * @param rhs : Scalar to multiply by.
     *
     * @return Reference to @a *this.
     */
    vec3& operator*=(double rhs);

    /**
     * @brief Scalar divide assignment operator
     *
     * Divide the current vector by @a rhs.
     *
     * @param rhs : Scalar to divide by.
     *
     * @return Reference to @a *this.
     */
    vec3& operator/=(double rhs);

    /**
     * @brief Negation operator
     *
     * Returns the opposite of a vector.
     *
     * @return Opposite of the current vector.
     */
    vec3 operator-();

    /**
     * @brief Vector length
     *
     * Returns the length of a vector, i.e. its norm.
     *
     * @return Norm of the current vector.
     */
    double length() const;

    double x; ///< x coordinate
    double y; ///< y coordinate
    double z; ///< z coordinate
};

/**
 * @brief Scalar multiply operator
 *
 * Multiply @a v by @a t and return the result.
 *
 * @param t : Scalar to multiply @a v by.
 * @param v : Vector to be scaled.
 *
 * @return Scaled vector \f$t\vec{v}\f$.
 */
vec3 operator*(double t, vec3 v);

/**
 * @brief Scalar divide operator
 *
 * Divide @a v by @a t and return the result.
 *
 * @param t : Scalar to divide @a v by.
 * @param v : Vector to be scaled.
 *
 * @return Scaled vector \f$\frac{1}{t}\vec{v}\f$.
 */
vec3 operator/(vec3 v, double t);

/**
 * @brief Vector addition operator
 *
 * Add @a rhs to @a lhs and return the result.
 *
 * @param lhs : First vector.
 * @param rhs : Second vector.
 *
 * @return Sum of @a lhs and @a rhs.
 */
vec3 operator+(vec3 lhs, const vec3& rhs);

/**
 * @brief Vector substraction operator
 *
 * Substract @a rhs from @a lhs and return the result.
 *
 * @param lhs : First vector.
 * @param rhs : Second vector.
 *
 * @return Difference of @a lhs and @a rhs.
 */
vec3 operator-(vec3 lhs, const vec3& rhs);

/**
 * @brief Dot product
 *
 * Returns the dot product of @a v1 and @a v2.
 *
 * @param v1 : First vector.
 * @param v2 : Second vector.
 *
 * @return Dot product of @a v1 and @a v2.
 */
double dot(const vec3& v1, const vec3& v2);

/**
 * @brief Cross product
 *
 * Returns the cross product of @a v1 and @a v2.
 *
 * @param v1 : First vector.
 * @param v2 : Second vector.
 *
 * @return Cross product of @a v1 and @a v2.
 */
vec3 cross(const vec3& v1, const vec3& v2);

/**
 * @brief Normalize vector
 *
 * Return the unit vector with the same direction as @a v.
 *
 * @pre @a v is assumed to be non-zero.
 *
 * @param v : Vector to normalize.
 *
 * @return Unit vector with the same direction as @a v.
 */
vec3 normalize(vec3 v);
