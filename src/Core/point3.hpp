/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Core/point3.hpp
 * @brief point3 class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of a class representing a point in a 3-dimensional space.
 *
 */

#pragma once

#include "vec3.hpp"

/**
 * @brief point3 class
 *
 * Represents a point in a 3-dimensional space.
 *
 */
struct point3
{
    /**
     * @brief Constructor
     *
     * Default constructor.
     *
     */
    point3() = default;

    /**
     * @brief Constructor
     *
     * Create a point from three coordinates.
     *
     * @param x : x coordinate.
     * @param y : y coordinate.
     * @param z : z coordinate.
     */
    point3(double x, double y, double z);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Point to copy.
     */
    point3(const point3& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~point3() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Point to assign to the current one.
     *
     * @return Reference to @a *this.
     */
    point3& operator=(const point3& rhs) = default;

    /**
     * @brief Addition assignment operator
     *
     * Add a vector to the current point.
     *
     * @param v : Vector to add.
     *
     * @return Reference to @a *this.
     */
    point3& operator+=(const vec3& v);

    /**
     * @brief Substraction assignment operator
     *
     * Substract a vector from the current point.
     *
     * @param v : Vector to substract.
     *
     * @return Reference to @a *this.
     */
    point3& operator-=(const vec3& v);

    double x; ///< x coordinate
    double y; ///< y coordinate
    double z; ///< z coordinate
};

/**
 * @brief Addition operator
 *
 * Add a point and a vector, returning a translated point.
 *
 * @param p : Point to translate.
 * @param v : Translation vector.
 *
 * @return Translated point \f$p+\vec{v}\f$.
 */
point3 operator+(point3 p, const vec3& v);

/**
 * @brief Substraction operator
 *
 * Substract a vector from a point, returning a translated point.
 *
 * @param p : Point to translate.
 * @param v : Translation vector.
 *
 * @return Translated point \f$p-\vec{v}\f$.
 */
point3 operator-(point3 p, const vec3& v);

/**
 * @brief Point substraction operator
 *
 * Substract point @a p1 to point @a p2, yielding the vector from @a p2 to
 * @a p1.
 *
 * @param p1 : Vector end.
 * @param p2 : Vector origin.
 *
 * @return Vector \f$\vec{p_2p_1}\f$.
 */
vec3 operator-(const point3& p1, const point3& p2);
