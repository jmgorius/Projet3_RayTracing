/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "point3.hpp"

point3::point3(double _x, double _y, double _z) :
    x(_x), y(_y), z(_z)
{
}

point3& point3::operator+=(const vec3& rhs)
{
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
}

point3& point3::operator-=(const vec3& rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
}

point3 operator+(point3 p, const vec3& v)
{
    p += v;
    return p;
}

vec3 operator-(const point3& p1, const point3& p2)
{
    return vec3(
               p1.x - p2.x,
               p1.y - p2.y,
               p1.z - p2.z
           );
}

point3 operator-(point3 p, const vec3& v)
{
    p -= v;
    return p;
}
