/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Core/color3f.hpp
 * @brief color3f class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of a double precision floating point representation of colors in
 * the RGB color space.
 *
 */

#pragma once

#include "color.hpp"

/**
 * @brief color3f class
 *
 * Represents a color using double precision floating point numbers between 0.0
 * and 1.0 in the RGB color space.
 *
 */
struct color3f
{
    /** White color (r = 1.0, g = 1.0, b = 1.0) */
    static color3f White;
    /** Black color (r = 0.0, g = 0.0, b = 0.0) */
    static color3f Black;
    /** Red color (r = 1.0, g = 0.0, b = 0.0) */
    static color3f Red;
    /** Green color (r = 0.0, g = 1.0, b = 0.0) */
    static color3f Green;
    /** Blue color (r = 0.0, g = 0.0, b = 1.0) */
    static color3f Blue;
    /** Yellow color (r = 1.0, g = 1.0, b = 0.0) */
    static color3f Yellow;
    /** Magenta color (r = 1.0, g = 0.0, b = 1.0) */
    static color3f Magenta;
    /** Cyan color (r = 0.0, g = 1.0, b = 1.0) */
    static color3f Cyan;

    /**
     * @brief Constructor
     *
     * Default constructor.
     *
     */
    color3f() = default;

    /**
     * @brief Constructor
     *
     * Create a color from red, green and blue components.
     *
     * @pre @a red, @a green and @a blue are assumed to be between 0.0 and 1.0.
     *
     * @param red : Red component.
     * @param green : Green component.
     * @param blue : Blue component.
     */
    color3f(double red, double green, double blue);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Color to copy.
     */
    color3f(const color3f& other) = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Color to assign to the current one.
     *
     * @return Reference to @a *this.
     */
    color3f& operator=(const color3f& rhs) = default;

    /**
     * @brief Convert to integer color
     *
     * Returns the integer-component color corresponding to the current color.
     *
     * @remark Use this method only before sending the color to the screen in
     * order to minimize rounding errors.
     *
     * @return Integer-component representation of the color.
     */
    rt::color to_rtcolor() const;

    double r; ///< Red component
    double g; ///< Green component
    double b; ///< Blue component
};
