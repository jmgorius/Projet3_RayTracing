/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "color3f.hpp"

#include <cstdint>
#include <algorithm>

color3f color3f::White(1.0, 1.0, 1.0);
color3f color3f::Black(0.0, 0.0, 0.0);
color3f color3f::Red(1.0, 0.0, 0.0);
color3f color3f::Green(0.0, 1.0, 0.0);
color3f color3f::Blue(0.0, 0.0, 1.0);
color3f color3f::Yellow(1.0, 1.0, 0.0);
color3f color3f::Magenta(1.0, 0.0, 1.0);
color3f color3f::Cyan(0.0, 1.0, 1.0);

color3f::color3f(double red, double green, double blue) :
    r(red), g(green), b(blue)
{
}

rt::color color3f::to_rtcolor() const
{
    uint8_t ir = static_cast<uint8_t>(std::min(255.0 * r, 255.0));
    uint8_t ig = static_cast<uint8_t>(std::min(255.0 * g, 255.0));
    uint8_t ib = static_cast<uint8_t>(std::min(255.0 * b, 255.0));
    return rt::color(ir, ig, ib);
}
