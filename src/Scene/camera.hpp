/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/camera.hpp
 * @brief Camera class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 2.1
 * @date December, 8th 2017
 *
 * Handle the display of the scene by projecting it onto a given viewing plane.
 *
 */

#pragma once

#include "point3.hpp"
#include "vec3.hpp"
#include "ray.hpp"

/**
 * @brief Camera class
 *
 * Defines the position, target point, up direction, field of view and
 * aspect ratio of the viewing area.
 *
 */
class Camera
{
public:
    /**
     * @brief Constructor
     *
     * Constructor to define the properties of a Camera object.
     *
     * @param position : Position in world space coordinates.
     * @param look_at : Coordinates of the target point.
     * @param up : Direction to be considered as up-facing.
     * @param vertical_fov : Vertical field of view.
     * @param aspect_ratio : Aspect ratio of the projection plane.
     */
    Camera(const point3& position, const point3& look_at, const vec3& up,
           double vertical_fov, double aspect_ratio);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Camera object to copy.
     */
    Camera(const Camera& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~Camera() = default;

    /**
     * @brief Get the ray associated to a point on the viewing plane
     *
     * The viewing plane is parameterized by two vectors \f$\vec{a}\f$ and \f$\vec{b}\f$,
     * which correspond respectively to the horizontal and the vertical
     * direction. This method returns the ray cast from the position of the
     * camera to the point \f$u\vec{a}+v\vec{b}\f$.
     *
     * @param u : Horizontal parameter.
     * @param v : Vertical parameter.
     *
     * @return The ray cast from the camera position.
     */
    Ray get_ray(double u, double v) const;

private:
    point3 m_origin; ///< Position of the camera, from which rays are emitted
    point3 m_upper_left_corner; ///< Upper left corner of the viewing plane
    vec3 m_horizontal; ///< Horizontal parametrization vector
    vec3 m_vertical; ///< Vertical parametrization vector
};
