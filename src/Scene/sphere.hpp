/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/sphere.hpp
 * @brief Sphere class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of a sphere shape.
 *
 */

#pragma once

#include "shape.hpp"
#include "point3.hpp"

/**
 * @brief Sphere class
 *
 * This class defines a sphere given its center and its radius.
 *
 */
class Sphere : public Shape
{
public:
    /**
     * @brief Constructor
     *
     * Constructor to define the properties of a Sphere object.
     *
     * @param center : Center of the sphere in world space coordinates
     * @param radius : Sphere radius.
     * @param color : Sphere color (RGB).
     */
    Sphere(const point3& center, double radius,
           const color3f& color);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Sphere to copy.
     */
    Sphere(const Sphere& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    virtual ~Sphere() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Sphere to assign to the current one.
     *
     * @return Reference to @a *this.
     */
    Sphere& operator=(const Sphere& rhs) = default;

    /**
     * @brief Check for intersection
     *
     * Checks wether an intersection with @a r occured during [@a tmin;@a tmax].
     *
     * @see Shape::intersects
     */
    bool intersects(const Ray & r,
                    double tmin, double tmax,
                    SurfaceInteraction& interaction) const final;

    /**
     * @brief Compute normal at given point
     *
     * Compute the normal to the sphere at @a p.
     *
     * @see Shape::compute_normal_at
     */
    vec3 compute_normal_at(const point3 & p) const final;

private:
    point3 m_center; ///< Sphere center in world space coordinates
    double m_radius; ///< Sphere radius
};
