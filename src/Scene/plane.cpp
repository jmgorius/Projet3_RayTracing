/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "plane.hpp"

Plane::Plane(const point3& point, const vec3& normal,
             const color3f& color) :
    Shape(color), m_pos(point), m_normal(normal)
{
}

bool Plane::intersects(const Ray& r,
                       double tmin, double tmax,
                       SurfaceInteraction& interaction) const
{
    // Use the fact the a plane containing a point p is the set of points q
    // verifying the equation dot(m_normal, p - q) = 0
    double dot_dir = dot(m_normal, r.direction);
    double dot_ray = dot(m_normal, m_pos - r.origin);

    // Exclude tangent rays
    if (dot_dir == 0)
        return false;

    // Intersection time
    double t = dot_ray / dot_dir;

    if(t < tmax && t > tmin)
    {
        interaction.time = t;
        interaction.point = r.point_at(interaction.time);
        interaction.normal = compute_normal_at(interaction.point);
        interaction.color = m_color;
        return true;
    }

    return false;
}

vec3 Plane::compute_normal_at(const point3& p) const
{
    return m_normal;
}
