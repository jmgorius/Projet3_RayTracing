#pragma once

#include "vector.hpp"
#include "color.hpp"
#include "ray.hpp"

using namespace rt;

class Hit
{
private:
	Ray gen;
	vector point;
	vector normal;
	color col;

public:
	Hit(const Ray& g, const vector& p, const vector& n, const color& c);
	
	Ray get_ray();
	vector get_point();
	vector get_normal();
	color get_color();
};

