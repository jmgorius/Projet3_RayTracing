/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/surface_interaction.hpp
 * @brief SurfaceInteraction class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 2.0
 * @date December, 7th 2017
 *
 * Definition of the class responsible for transmitting interaction information.
 *
 */

#pragma once

#include "point3.hpp"
#include "vec3.hpp"
#include "color3f.hpp"

/**
 * @brief SurfaceInteraction class
 *
 * This class is used to pass information about interactions between a ray and
 * a surface during and/or after a calculation involving intersection tests.
 *
 */
struct SurfaceInteraction
{
    /**
     * @brief Constructor
     *
     * Default constructor.
     *
     */
    SurfaceInteraction() = default;

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     */
    SurfaceInteraction(const SurfaceInteraction&) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~SurfaceInteraction() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     */
    SurfaceInteraction& operator=(const SurfaceInteraction&) = default;

    point3 point; ///< Point at which the intersection is considered
    vec3 normal; ///< Normal to the shape on which the interaction happened
    double time; ///< Time at which the interaction occured
    color3f color; ///< Color of the surface
};
