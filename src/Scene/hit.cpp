#include "hit.hpp"

Hit::Hit(const Ray& g, const vector& p, const vector& n, const color& c):
    gen(g), point(p), normal(n), col(c)
{}

Ray Hit::get_ray()
{
	return gen;
}

vector Hit::get_point()
{
	return point;
}

vector Hit::get_normal()
{
	return normal;
}

color Hit::get_color()
{
	return col;
}
