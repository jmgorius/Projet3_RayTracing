/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "sphere.hpp"

#include <cmath>

Sphere::Sphere(const point3& center, double radius,
               const color3f& color) :
    Shape(color), m_center(center), m_radius(radius)
{
}

bool Sphere::intersects(const Ray& r,
                        double tmin, double tmax,
                        SurfaceInteraction& interaction) const
{
    vec3 ac = m_center - r.origin;
    // a = dot(r.direction, r.direction) = 1
    double b = -2.0 * dot(ac, r.direction);
    double c = dot(ac, ac) - m_radius * m_radius;
    double delta = b * b - 4.0 * c;

    // Solve quadratic equation to determine intersection time
    if(delta > 0.0)
    {
        // Test first root
        double t0 = (-b - sqrt(delta)) / 2.0;
        if(t0 < tmax && t0 > tmin)
        {
            interaction.time = t0;
            interaction.point = r.point_at(interaction.time);
            interaction.normal = compute_normal_at(interaction.point);
            interaction.color = m_color;
            return true;
        }

        // Test second root
        double t1 = (-b + sqrt(delta)) / 2.0;
        if(t1 < tmax && t1 > tmin)
        {
            interaction.time = t1;
            interaction.point = r.point_at(interaction.time);
            interaction.normal = compute_normal_at(interaction.point);
            interaction.color = m_color;
            return true;
        }
    }

    return false;
}

vec3 Sphere::compute_normal_at(const point3& p) const
{
    return normalize(p - m_center);
}
