/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/ray.hpp
 * @brief Ray class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of a class representing rays cast into the scene.
 *
 */

#pragma once

#include "point3.hpp"
#include "vec3.hpp"

/**
 * @brief Ray class
 *
 * This class represents a ray by an origin and a normalized direction vector.
 * Any point along the ray can be obtained thanks to this parametrization.
 *
 */
struct Ray
{
public:
    /**
     * @brief Constructor
     *
     * Default constructor.
     *
     */
    Ray() = default;

    /**
     * @brief Constructor
     *
     * Constructor to define the properties of a Ray.
     *
     * \remark This constructor normalizes the given direction vector.
     *
     * @param origin : Origin of the ray.
     * @param direction : Ray direction.
     */
    Ray(const point3& origin, const vec3& direction);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Ray to copy.
     */
    Ray(const Ray& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~Ray() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Ray to assign to the current one.
     */
    Ray& operator=(const Ray& rhs) = default;

    /**
     * @brief Get point on ray
     *
     * Returns the point \f$p\f$ on the ray given by \f$p=o+t\vec{d}\f$, where
     * \f$o\f$ is the origin of the ray and \f$\vec{d}\f$ is its direction.
     *
     * @param t : Time parameter.
     *
     * @return Point on ray at time @a t.
     */
    point3 point_at(double t) const;

    point3 origin; ///< Ray origin
    vec3 direction; ///< Ray direction
};
