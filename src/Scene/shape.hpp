/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/shape.hpp
 * @brief Shape class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.4
 * @date December, 6th 2017
 *
 * Definition of shapes.
 *
 */

#pragma once

#include "surface_interaction.hpp"
#include "ray.hpp"

/**
 * @brief Shape class
 *
 * Abstract base class for every shape.
 *
 * @see Sphere
 * @see Plane
 */
class Shape
{
public:
    /**
     * @brief Constructor
     *
     * Defines a shape's color in the RGB color space.
     *
     * @param color : Shape color.
     */
    explicit Shape(const color3f& color) :
        m_color(color) {}

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Shape to copy.
     */
    Shape(const Shape& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    virtual ~Shape() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Shape to assign to the current one.
     */
    Shape& operator=(const Shape& rhs) = default;

    /**
     * @brief Check for intersection
     *
     * Checks wether the ray @a r intersects the shape between time @a tmin and
     * time @a tmax. This function fills in @a interaction with the surface
     * interaction information corresponding to the intersection point,
     * if one exists.
     *
     * @param r : Ray to test intersection against.
     * @param tmin : Lower time bound for the intersection test.
     * @param tmax : Upper time bound for intersection test.
     * @param interaction : Surface interaction information to return if an
     *                      intersection occurs.
     *
     * @return true if an intersection occurs during [@a tmin;@a tmax],
     * false otherwise.
     *
     * @see SurfaceInteraction
     */
    virtual bool intersects(const Ray& r,
                            double tmin, double tmax,
                            SurfaceInteraction& interaction) const = 0;

    /**
     * @brief Compute normal at given point
     *
     * Returns the normal to the shape at the given point @a p.
     *
     * @pre @a p is assumed to be a point on the shape.
     *
     * @param p : Point at which to compute the normal.
     *
     */
    virtual vec3 compute_normal_at(const point3& p) const = 0;

    /**
     * @brief Get color
     *
     * Returns the color of the shape in the RGB color space.
     *
     * @return Color of the shape.
     */
    inline const color3f& color() const {
        return m_color;
    }

protected:
    color3f m_color; ///< Shape color
};
