/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "camera.hpp"

#include <cmath>

Camera::Camera(const point3& position, const point3& look_at,
               const vec3& up,
               double vertical_fov, double aspect_ratio)
{
    double theta = vertical_fov * M_PI / 180.0; // Convert to radians
    double half_height = tan(theta / 2.0);
    double half_width = aspect_ratio * half_height;

    m_origin = position;

    // Compute the camera's local coordinate system
    // Note: one axis is pointing towards us
    vec3 view = -normalize(look_at - position);
    vec3 cam_right = normalize(cross(view, up));
    vec3 cam_down = cross(view, cam_right);

    // Compute parametrization of the viewing plane
    m_upper_left_corner = m_origin - half_width * cam_right - half_height * cam_down - view;
    m_horizontal = 2.0 * half_width * cam_right;
    m_vertical = 2.0 * half_height * cam_down;
}

Ray Camera::get_ray(double u, double v) const
{
    // Compute the target point on the viewing plane
    point3 screen_pt = m_upper_left_corner + u * m_horizontal + v * m_vertical;
    return Ray(m_origin, screen_pt - m_origin);
}
