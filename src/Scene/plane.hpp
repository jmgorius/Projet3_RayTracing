/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/plane.hpp
 * @brief Plane class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.0
 * @date December, 6th 2017
 *
 * Definition of an infinite plane shape.
 *
 */

#pragma once

#include "shape.hpp"
#include "point3.hpp"
#include "vec3.hpp"

/**
 * @brief Plane class
 *
 * This class defines an infinite plane given one of its points and a normal
 * vector.
 *
 */
class Plane : public Shape
{
public:
    /**
     * @brief Constructor
     *
     * Constructor to define the properties of a Plane object.
     *
     * @param point : A point of the plane.
     * @param normal : The plane's surface normal.
     * @param color : The plane's color, in the RGB color space.
     */
    Plane(const point3& point, const vec3& normal,
          const color3f& color);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Plane to copy.
     */
    Plane(const Plane& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    virtual ~Plane() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Plane to assign to the current one.
     *
     * @return Reference to @a *this.
     */
    Plane& operator=(const Plane& rhs) = default;

    /**
     * @brief Check for intersection
     *
     * Checks wether an intersection with @a r occured during [@a tmin;@a tmax].
     *
     * @see Shape::intersects
     */
    bool intersects(const Ray & r,
                    double tmin, double tmax,
                    SurfaceInteraction& interaction) const final;

    /**
     * @brief Compute normal at given point
     *
     * Compute the normal to the plane at @a p.
     *
     * @see Shape::compute_normal_at
     */
    vec3 compute_normal_at(const point3 & p) const final;

private:
    point3 m_pos; ///< Point on a plane
    vec3 m_normal; ///< Normal vector
};
