/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/scene.hpp
 * @brief Scene class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 2.2
 * @date December, 8th 2017
 *
 * Definition of the class handling the scene rendering.
 *
 */

#pragma once

#include <vector>
#include <memory>

#include "sphere.hpp"
#include "shape.hpp"
#include "light.hpp"
#include "camera.hpp"
#include "screen.hpp"

/**
 * @brief Scene class
 *
 * Manages the whole scene by maintaining a collection of shapes and lights, as
 * well as the camera used for the rendering process.
 *
 */
class Scene
{
public:
    /**
     * @brief Constructor
     *
     * Constructor to define the properties of a Scene.
     *
     * @param camera : Camera used for rendering.
     * @param shapes : Collection of shapes to render.
     * @param lights : Collection of lights illuminating the scene.
     */
    Scene(const Camera& camera,
          const std::vector<std::shared_ptr<Shape>>& shapes,
          const std::vector<std::shared_ptr<Light>>& lights);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Scene to copy.
     */
    Scene(const Scene& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~Scene() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Scene to assign to the current one.
     */
    Scene& operator=(const Scene& rhs) = default;

    /**
     * @brief Render the scene to the screen
     *
     * Renders the scene to the screen handled by @a s, using @a num_samples
     * samples per pixel. The rendering arguments are mainly defined by the
     * camera passed to the constructor.
     *
     * @param s : Screen to render the scene to.
     * @param num_samples : Number of samples per pixel.
     */
    void render(rt::screen& s, unsigned int num_samples = 8);

private:
    /**
     * @brief Compute the color corresponding to a given ray
     *
     * Computes the color obtained by casting @a r into the scene and using the
     * color and light information given by its intersections with objects and
     * by the lights placed in the scene.
     *
     * @param r : Ray cast into the scene.
     *
     * @return Color corresponding to @a r.
     */
    color3f compute_color(const Ray& r) const;

private:
    Camera m_camera; ///< Rendering camera.
    std::vector<std::shared_ptr<Shape>> m_shapes; ///< Collection of shapes
    std::vector<std::shared_ptr<Light>> m_lights; ///< Collection of light
    ///< sources.
};
