/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

/**
 * @file Scene/light.hpp
 * @brief Light class definition.
 * @authors Tom Bachard, Marco Freire, Jean-Michel Gorius
 * @version 1.3
 * @date December, 7th 2017
 *
 * Definition of the class responsible for handling lights.
 *
 */

#pragma once

#include "point3.hpp"
#include "color3f.hpp"

/**
 * @brief Light class
 *
 * This class handles the properties of a light object, namely its position,
 * color and power. The power of a light decreases with the square of the
 * distance between this light and the considered point.
 *
 */
class Light
{
public:
    /**
     * @brief Constructor
     *
     * Constructor to define the properties of the Light object.
     *
     * @param position : Position of the light source in world space coordinates
     * @param color : Color of the light (RGB).
     * @param power : Power of the light source.
     */
    Light(const point3& position, const color3f& color, double power);

    /**
     * @brief Copy constructor
     *
     * Default copy constructor.
     *
     * @param other : Light object to copy.
     */
    Light(const Light& other) = default;

    /**
     * @brief Destructor
     *
     * Default destructor.
     *
     */
    ~Light() = default;

    /**
     * @brief Assignment operator
     *
     * Default assignment operator.
     *
     * @param rhs : Light source to be assigned to the current one.
     *
     * @return Reference to @a *this.
     */
    Light& operator=(const Light& rhs) = default;

    /**
     * @brief Get position
     *
     * Returns the position of the light source in world space coordinates.
     *
     * @return Light position.
     */
    inline const point3& position() const {
        return m_position;
    }

    /**
     * @brief Get color
     *
     * Returns the color of the light source, in the RGB color space.
     *
     * @return Light color.
     */
    inline const color3f& color() const {
        return m_color;
    }

    /**
     * @brief Get power
     *
     * Returns the power of the light source.
     *
     * @return Light power.
     */
    inline double power() const {
        return m_power;
    }

private:
    point3 m_position; ///< Position in world space coordinates
    color3f m_color; ///< RGB color
    double m_power; ///< Power of the source
};
