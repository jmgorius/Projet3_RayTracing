/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include "scene.hpp"

#include <limits>
#include <cmath>
#include <cstdlib>

Scene::Scene(const Camera& camera,
             const std::vector<std::shared_ptr<Shape> >& shapes,
             const std::vector<std::shared_ptr<Light> >& lights) :
    m_camera(camera), m_shapes(shapes), m_lights(lights)
{
}

void Scene::render(rt::screen& s, unsigned int num_samples)
{
    const size_t width = static_cast<size_t>(s.width());
    const size_t height = static_cast<size_t>(s.height());

    // Traverse the screen line by line
    for(size_t y = 0; y < height; ++y)
    {
        for(size_t x = 0; x < width; ++x)
        {
            color3f pixel_color(0.0, 0.0, 0.0);

            // Supersampling
            for(size_t sample = 0; sample < num_samples; ++sample)
            {
                // Cast a camera ray starting from a random position around the
                // current pixel
                double orig_x = static_cast<double>(x) - 0.5 + drand48();
                double orig_y = static_cast<double>(y) - 0.5 + drand48();
                double u = orig_x /
                           static_cast<double>(s.width());
                double v = orig_y /
                           static_cast<double>(s.height());
                Ray cam_ray = m_camera.get_ray(u, v);

                // Compute the color corresponding to the current ray and add it
                // to the resulting color
                color3f computed_color = compute_color(cam_ray);
                pixel_color.r += computed_color.r;
                pixel_color.g += computed_color.g;
                pixel_color.b += computed_color.b;
            }
            // Scale by number of num_samples
            double ns = static_cast<double>(num_samples);
            pixel_color.r /= ns;
            pixel_color.g /= ns;
            pixel_color.b /= ns;

            // Write pixel to the screen
            s.set_pixel(x, y, pixel_color.to_rtcolor());
        }
    }
}

color3f Scene::compute_color(const Ray& r) const
{
    color3f result(color3f::Black);

    // Search for closest interaction point
    SurfaceInteraction interaction;
    // Note: Using std::numeric_limits<double>::infinity() can be problematic
    //       for comparisons
    double t = std::numeric_limits<double>::max();
    for(const std::shared_ptr<Shape>& shape : m_shapes)
    {
        if(shape->intersects(r, 0.001, t, interaction))
            t = interaction.time;
    }

    // If an intersection occured...
    if(t != std::numeric_limits<double>::max())
    {
        // Compute contribution of each light source to the current pixel color
        for(const std::shared_ptr<Light>& light : m_lights)
        {
            vec3 to_light = light->position() - interaction.point;
            double dist_to_light = to_light.length();
            to_light /= dist_to_light; // Normalize

            // The light must be on the same side of the surface as the normal
            if(dot(interaction.normal, to_light) <= 0.0)
                continue;

            // Trace ray to light source to check if it is visible from the
            // current intersection point
            Ray light_ray(interaction.point, to_light);
            SurfaceInteraction light_interaction; // Dummy interaction object
            bool in_shadow = false;
            for(const std::shared_ptr<Shape>& shape: m_shapes)
            {
                // Check wether the light ray is blocked by a shape
                if(shape->intersects(light_ray, 0.001, dist_to_light, light_interaction))
                {
                    in_shadow = true;
                    break;
                }
            }

            // Compute color
            if(!in_shadow)
            {
                // The light contribution decreases with the square of the
                // distance to the source
                double light_contribution =
                    light->power() / (dist_to_light * dist_to_light);

                // See https://en.wikipedia.org/wiki/Lambertian_reflectance
                double lambert_coefficient = std::max(0.0, dot(to_light, interaction.normal));
                result.r += light_contribution * lambert_coefficient * interaction.color.r * light->color().r;
                result.g += light_contribution * lambert_coefficient * interaction.color.g * light->color().g;
                result.b += light_contribution * lambert_coefficient * interaction.color.b * light->color().b;
            }
        }
        // Normalize light power contribution (see Maxwell's equations)
        result.r /= 4.0 * M_PI;
        result.g /= 4.0 * M_PI;
        result.b /= 4.0 * M_PI;
    }

    return result;
}
