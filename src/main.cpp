/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2017 Tom Bachard, Marco Freire and Jean-Michel Gorius
 */

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <cassert>
#include <vector>

#include "image.hpp"
#include "screen.hpp"
#include "color.hpp"
#include "scene.hpp"
#include "sphere.hpp"
#include "plane.hpp"

static void draw_rectangle(rt::screen& s, int x, int y, int width,
                           rt::color c)
{
    s.fill_rect(x, y, x + width - 1, y + width - 1, c);
}

static void draw_centered_cross(rt::screen& s, int x, int y,
                                int width, rt::color c) {
    assert(width > 0);
    int my_x = x - width/2;
    int my_y = y - width/2;
    //(s, my_x, my_y, width, c);
    draw_rectangle(s, my_x, my_y, width, c);
    draw_rectangle(s, my_x + width, my_y, width, c);
    draw_rectangle(s, my_x - width, my_y, width, c);
    draw_rectangle(s, my_x, my_y + width, width, c);
    draw_rectangle(s, my_x, my_y - width, width, c);
    s.set_pixel(x, y, rt::color::RED);
}

int main(int argc, char *argv[])
{
    std::clog << "Argument number: " << argc << std::endl;
    for (int i = 0; i < argc; i++)
        std::clog << "Argument " << i << ": " << argv[i] << std::endl;
    std::clog << std::string(50, '_') << std::endl;

    rt::screen s(640,480);

    Sphere sphere1(point3(0.0, 3.0, 4.0), 2.0, color3f(0.8, 0.8, 0.8));
    Plane plane1(point3(0.0, -4.0, 0.0), vec3(0.0, 1.0, 0.0),
                 color3f(0.5, 0.5, 0.5));
//     Plane plane2(point3(4.25, 2.0, 10.0), -vec3(1.2, 0.45, 2.03),
//              std::make_shared<Material>(Material(color3f::Magenta, 0.5)));
//     Plane plane3(point3(0.0, 0.0, 20.0), cross(-vec3(1.2, 0.45, 2.03), vec3(0.0, 1.0, 0.0)),
//              std::make_shared<Material>(Material(color3f::Yellow, 0.5)));
//
//     Sphere sphere2(point3(5.0, 3.0, 0.0), 3.0,
//             std::make_shared<Material>(Material(color3f::Green, 0.5)));
//     Sphere sphere3(point3(3.2, 1.5, 1.0), 1.5,
//             std::make_shared<Material>(Material(color3f::Blue, 0.5)));
//
    Light light1(point3(-4.0, 3.0, -1.0), color3f(1.0, 0.0, 0.0), 500.0);
    Light light2(point3(6.0, 2.0, -1.0), color3f(0.0, 1.0, 0.0), 500.0);

    Camera cam(
        point3(0.0, 5.0, -5.0),
        point3(3.0, 0.0, 1.0),
        vec3(0.0, 1.0, 0.0),
        90,
        static_cast<double>(s.width()) / static_cast<double>(s.height()));

    Scene scene(cam,
    {   std::make_shared<Sphere>(sphere1),
        std::make_shared<Plane>(plane1),
        //std::make_shared<Plane>(plane2),
        //std::make_shared<Plane>(plane3),
        //std::make_shared<Sphere>(sphere2),
        //std::make_shared<Sphere>(sphere3)
    },
    {   std::make_shared<Light>(light1),
        std::make_shared<Light>(light2)
    });

    scene.render(s, 10);

    s.update();
    
    // wait for the screen to be closed
    std::clog << "Waiting for QUIT event..." << std::endl;
    while(not s.wait_quit_event()) {}

    return 0;
}
