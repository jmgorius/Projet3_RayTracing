# [PROG1] Project 3 : Raytracing in C++

This project is the result of our work for the PROG1 class at ENS Rennes. It
implements a simple ray tracing program, which is able to handle spheres,
infinite planes and ponctual light sources. We only implemented the Lambertian
reflectance model to keep things as simple as possible.

## How to compile

To compile the project, simply place yourself into the root directory and run
make.
```
cd $(RAYTRACER_ROOT)
make
```
The main executable is named `raytracer`. You will find it in the `bin`
directory. Doxygen documentation is generated in the `doc/html` folder. Open
`doc/html/index.html` to go to the main documentation page.

## License

This work is licensed under the GNU General Public License (GPL) v3.0. See the
`LICENSE` file for more details.

## Authors

You can contact the authors using the following email adresses:
* Tom Bachard: <tom.bachard@ens-rennes.fr>
* Marco Freire:  <marco.freire@ens-rennes.fr>
* Jean-Michel Gorius: <jean-michel.gorius@ens-rennes.fr>
